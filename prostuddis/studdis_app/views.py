from django.contrib.auth.models import User
from django.shortcuts import render
from datetime import datetime

from .models import User, SchoolProfile, TeacherProfile, StudentProfile, ClassNum, ClassJournal, RoomNum, Lesson
from .serializers import UserSerializer, SchoolProfileSerializer, TeacherProfileSerializer, StudentProfileSerializer, \
    ClassNumSerializer, RoomNumSerializer, LessonSerializer
from .permissions import IsSchoolProfileOwner, IsTeacherProfileOwner, IsStudentProfileOwner

import django_filters
from rest_framework import viewsets
from rest_framework import status, permissions
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication
from rest_framework import filters
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.authtoken.models import Token

from _collections import OrderedDict


class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()


# PAGE NUMERATION
class Pagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 200

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('page_count', self.page.paginator.num_pages),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data),
        ]))


class SchoolProfileView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsSchoolProfileOwner, ]
    serializer_class = SchoolProfileSerializer
    queryset = SchoolProfile.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('user_pro', 'name_of_school', 'address_of_school', 'phone_of_school')
    search_fields = ('user_pro', 'name_of_school', 'address_of_school', 'phone_of_school')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = SchoolProfile.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('name_of_school'):
            filter_fields['name_of_school'] = self.request.GET.get('name_of_school')

        if self.request.GET.get('address_of_school'):
            filter_fields['address_of_school'] = self.request.GET.get('address_of_school')

        if self.request.GET.get('phone_of_school'):
            filter_fields['phone_of_school'] = self.request.GET.get('phone_of_school')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        # start_date = self.request.GET.get('start_date')
        # end_date = self.request.GET.get('end_date')
        # currency = self.request.GET.get('currency')
        # accounts = self.request.GET.get('accounts')
        #
        # if start_date and end_date:
        #     start_date = datetime.strptime(start_date, '%d.%m.%Y')
        #     end_date = datetime.strptime(end_date, '%d.%m.%Y')
        #     queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = SchoolProfileSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class TeacherProfileView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsTeacherProfileOwner, ]
    serializer_class = TeacherProfileSerializer
    queryset = TeacherProfile.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('user_pro', 'name_of_teacher', 'experience', 'lesson')
    search_fields = ('user_pro', 'name_of_teacher', 'experience', 'lesson',)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = TeacherProfile.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('name_of_teacher'):
            filter_fields['name_of_teacher'] = self.request.GET.get('name_of_teacher')

        if self.request.GET.get('experience'):
            filter_fields['experience'] = self.request.GET.get('experience')

        if self.request.GET.get('lesson'):
            filter_fields['lesson'] = self.request.GET.get('lesson')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        # start_date = self.request.GET.get('start_date')
        # end_date = self.request.GET.get('end_date')
        # currency = self.request.GET.get('currency')
        # accounts = self.request.GET.get('accounts')
        #
        # if start_date and end_date:
        #     start_date = datetime.strptime(start_date, '%d.%m.%Y')
        #     end_date = datetime.strptime(end_date, '%d.%m.%Y')
        #     queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = TeacherProfileSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class StudentProfileView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsStudentProfileOwner, ]
    serializer_class = StudentProfileSerializer
    queryset = StudentProfile.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('user_pro', 'name_of_student', 'class_num')
    search_fields = ('user_pro', 'name_of_student', 'class_num')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = StudentProfile.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('name_of_student'):
            filter_fields['name_of_student'] = self.request.GET.get('name_of_student')

        if self.request.GET.get('class_num'):
            filter_fields['class_num'] = self.request.GET.get('class_num')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        # start_date = self.request.GET.get('start_date')
        # end_date = self.request.GET.get('end_date')
        # currency = self.request.GET.get('currency')
        # accounts = self.request.GET.get('accounts')
        #
        # if start_date and end_date:
        #     start_date = datetime.strptime(start_date, '%d.%m.%Y')
        #     end_date = datetime.strptime(end_date, '%d.%m.%Y')
        #     queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = StudentProfileSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class ClassNumView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    serializer_class = ClassNumSerializer
    queryset = ClassNum.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('user_pro', 'name_of_class', 'class_teacher', 'room')
    search_fields = ('user_pro', 'name_of_class', 'class_teacher', 'room')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = ClassNum.objects.all()
        order_field = self.request.GET.get('order')
        filter_fields = {}

        if self.request.GET.get('name_of_class'):
            filter_fields['name_of_class'] = self.request.GET.get('name_of_class')

        if self.request.GET.get('class_teacher'):
            filter_fields['class_teacher'] = self.request.GET.get('class_teacher')

        if self.request.GET.get('room'):
            filter_fields['room'] = self.request.GET.get('room')

        if order_field:
            queryset = queryset.order_by(order_field)

        if filter_fields:
            queryset = queryset.filter(**filter_fields)

        # start_date = self.request.GET.get('start_date')
        # end_date = self.request.GET.get('end_date')
        class_teacher = self.request.GET.get('class_teacher')
        room = self.request.GET.get('room')
        #
        # if start_date and end_date:
        #     start_date = datetime.strptime(start_date, '%d.%m.%Y')
        #     end_date = datetime.strptime(end_date, '%d.%m.%Y')
        #     queryset = queryset.filter(date__date__gte=start_date, date__date__lte=end_date)

        if class_teacher:
            queryset = queryset.filter(class_teacher=class_teacher)

        if room:
            queryset = queryset.filter(room=room)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = ClassNumSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class RoomNumView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    serializer_class = RoomNumSerializer
    queryset = RoomNum.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('user_pro', 'name_of_class', 'class_teacher', 'room')
    search_fields = ('user_pro', 'name_of_class', 'class_teacher', 'room')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = RoomNum.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = RoomNumSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)


class LessonView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    serializer_class = LessonSerializer
    queryset = Lesson.objects.all()
    lookup_field = 'pk'
    filter_backends = ([django_filters.rest_framework.DjangoFilterBackend, filters.SearchFilter])
    filter_fields = ('user_pro', 'name_of_lesson')
    search_fields = ('user_pro', 'name_of_lesson')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request_user'] = self.request.user
        return context

    # Sort the products py their qualification
    def get_queryset(self):
        queryset = Lesson.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = LessonSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)



