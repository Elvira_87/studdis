from rest_framework.routers import DefaultRouter
from django.urls import path
from .views import UserViewSet, SchoolProfileView, TeacherProfileView, StudentProfileView, ClassNumView, RoomNumView, \
    LessonView


urlpatterns = [
    path('user/', UserViewSet.as_view()),
    path('school_profile/', SchoolProfileView.as_view({'post': 'create'})),
    path('school_profile/<int:pk>/',
         SchoolProfileView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),

    path('teacher/', TeacherProfileView.as_view({'post': 'create'})),
    path('teacher/<int:pk>/',
         TeacherProfileView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('teacher/lesson/', LessonView.as_view({'get': 'list'})),

    path('student/', StudentProfileView.as_view({'post': 'create'})),
    path('student/<int:pk>/',
         StudentProfileView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('student/class_num/', ClassNumView.as_view({'get': 'list'})),

    path('class_num/', ClassNumView.as_view({'get': 'list', 'post': 'create'})),
    path('class_num/<int:pk>/', ClassNumView.as_view({'get': 'retrieve', 'delete': 'destroy', 'put': 'update'})),
    path('class_num/room_num/', RoomNumView.as_view({'get': 'list'})),

]