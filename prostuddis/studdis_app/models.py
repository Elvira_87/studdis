from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_school = models.BooleanField()
    is_teacher = models.BooleanField()
    is_student = models.BooleanField()

    def __str__(self):
        return f'{str(self.username)}'


class SchoolProfile(models.Model):
    class Meta:
        verbose_name = 'Главный Аккаунт'
        verbose_name_plural = 'Главные Аккаунты'
        ordering = ['-id']

    user_pro = models.OneToOneField(User, verbose_name='Пользователь', on_delete=models.CASCADE, blank=True, null=True)
    name_of_school = models.CharField(verbose_name='Название школы', max_length=128, blank=True, null=True)
    address_of_school = models.CharField(verbose_name='Адрес школы', max_length=132, blank=True, null=True)
    phone_of_school = models.CharField(verbose_name='Телефон', max_length=15, blank=True, null=True)

    def __str__(self):
        return f'{str(self.name_of_school)}'


class TeacherProfile(models.Model):
    class Meta:
        verbose_name = 'Аккаунт Учителя'
        verbose_name_plural = 'Аккаунты Учителей'
        ordering = ['-id']

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='teacher',
                                 on_delete=models.CASCADE,
                                 blank=True, null=True)
    name_of_teacher = models.CharField(verbose_name='ФИО', max_length=128, blank=True, null=True)
    experience = models.CharField(verbose_name='Опыт работы', max_length=128, blank=True, null=True)
    photo = models.ImageField(verbose_name='Фото', upload_to='Фото', blank=True, null=True)
    lesson = models.ForeignKey('Lesson', verbose_name='Урок', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f'{str(self.name_of_teacher)}'


class StudentProfile(models.Model):
    class Meta:
        verbose_name = 'Аккаунт Ученика'
        verbose_name_plural = 'Аккаунты Учеников'
        ordering = ['-id']

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='student',
                                 on_delete=models.CASCADE, blank=True, null=True)
    photo = models.ImageField(verbose_name='Фото', upload_to='Фото', blank=True, null=True)
    name_of_student = models.CharField(verbose_name='ФИО', max_length=128, blank=True, null=True)
    class_num = models.ForeignKey('ClassNum', verbose_name='Класс', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f'{str(self.name_of_student)}'


class ClassNum(models.Model):
    class Meta:
        verbose_name = 'Класс'
        verbose_name_plural = 'Классы'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='class_num',
                                 on_delete=models.CASCADE, blank=True, null=True)
    name_of_class = models.CharField(verbose_name='Название Класса', max_length=128, blank=True, null=True)
    class_teacher = models.ForeignKey(TeacherProfile, verbose_name='Куратор класса', on_delete=models.CASCADE,
                                      blank=True, null=True)
    room = models.ForeignKey('RoomNum', verbose_name='Кабинет класса', on_delete=models.CASCADE, blank=True,
                             null=True)

    def __str__(self):
        return f'{str(self.name_of_class)}'


class RoomNum(models.Model):
    class Meta:
        verbose_name = 'Кабинет'
        verbose_name_plural = 'Кабинеты'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='room_num',
                                 on_delete=models.CASCADE, blank=True, null=True)
    name_of_room = models.CharField(verbose_name='Название Кабинета', max_length=128, blank=True, null=True)

    def __str__(self):
        return f'{str(self.name_of_room)}'


class Lesson(models.Model):
    class Meta:
        verbose_name = 'Урок'
        verbose_name_plural = 'Уроки'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='lesson',
                                 on_delete=models.CASCADE, blank=True, null=True)
    name_of_lesson = models.CharField(verbose_name='Название Урока', max_length=128, blank=True, null=True)

    def __str__(self):
        return f'{str(self.name_of_lesson)}'


class Dashboard(models.Model):
    class Meta:
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписания'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='dashboard',
                                 on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField('Дата регистрации', auto_now_add=True, blank=True, null=True)
    name_of_lesson = models.ForeignKey(Lesson, verbose_name='Название Урока', on_delete=models.CASCADE, blank=True,
                                       null=True)
    s_time = models.DateTimeField('Дата регистрации', auto_now_add=True, blank=True, null=True)
    f_time = models.DateTimeField('Дата регистрации', auto_now_add=True, blank=True, null=True)
    teacher_name = models.ForeignKey(TeacherProfile, verbose_name='Куратор класса', on_delete=models.CASCADE,
                                     blank=True, null=True)
    room_num = models.ForeignKey(RoomNum, verbose_name='Кабинет класса', on_delete=models.CASCADE, blank=True,
                                 null=True)
    class_num = models.ForeignKey(ClassNum, verbose_name='Класс', on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f'{str(self.date)}'


class ClassJournal(models.Model):
    class Meta:
        verbose_name = 'Классный Журнал'
        verbose_name_plural = 'Классные Журналы'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='journal',
                                 on_delete=models.CASCADE, blank=True, null=True)
    class_num = models.ForeignKey(ClassNum, verbose_name='Класс', on_delete=models.CASCADE, blank=True, null=True)
    date = models.DateTimeField('Дата регистрации', auto_now_add=True, blank=True, null=True)
    lesson = models.ForeignKey(Lesson, verbose_name='Урок', on_delete=models.CASCADE, blank=True, null=True)
    student_name = models.ForeignKey(StudentProfile, verbose_name='Ученик', on_delete=models.CASCADE, blank=True,
                                     null=True)
    grade = models.PositiveSmallIntegerField(verbose_name='Оценка', default=0)

    def __str__(self):
        return f'{str(self.class_num)}'


class Grade(models.Model):
    class Meta:
        verbose_name = 'Классный Журнал'
        verbose_name_plural = 'Классные Журналы'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='grade', on_delete=models.CASCADE,
                                 blank=True, null=True)
    grades = models.PositiveSmallIntegerField(verbose_name='Оценка', default=0)
    quarter_grade = models.PositiveSmallIntegerField(verbose_name='Оценка', default=0)
    semester_grade = models.PositiveSmallIntegerField(verbose_name='Оценка', default=0)
    average_grade = models.PositiveSmallIntegerField(verbose_name='Оценка', default=0)

    def __str__(self):
        return f'{str(self.grades)}'


class StudyingPlan(models.Model):
    class Meta:
        verbose_name = 'Учебный план'
        verbose_name_plural = 'Учебные планы'

    week = 'Неделя'
    month = 'Месяц'
    quarter = 'Четверть'
    PERIOD_CHOICES = (
        (week, 'Неделя'),
        (month, 'Месяц'),
        (quarter, 'Четверть'),
    )

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='plan', on_delete=models.CASCADE,
                                 blank=True, null=True)
    date = models.DateTimeField('Дата регистрации', auto_now_add=True, blank=True, null=True)
    period = models.CharField(verbose_name='Вид', max_length=32, choices=PERIOD_CHOICES)
    lesson = models.ForeignKey(Lesson, verbose_name='Урок', on_delete=models.CASCADE, blank=True, null=True)
    class_num = models.ForeignKey(ClassNum, verbose_name='Класс', on_delete=models.CASCADE, blank=True, null=True)
    subject = models.CharField(verbose_name='Тема Урока', max_length=280, blank=True, null=True)

    def __str__(self):
        return f'{str(self.subject)}'


class Attendance(models.Model):
    class Meta:
        verbose_name = 'Посещаемость ученика'
        verbose_name_plural = 'Посещаемость учеников'

    present = 'Присутствует'
    absent = 'Отсутсвует'
    late = 'Опоздал'
    ATTEND_CHOICES = (
        (present, 'Присутствует'),
        (absent, 'Отсутсвует'),
        (late, 'Опоздал'),
    )

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='attendance', on_delete=models.CASCADE,
                                 blank=True, null=True)
    class_num = models.ForeignKey(ClassNum, verbose_name='Класс', on_delete=models.CASCADE, blank=True, null=True)
    student_name = models.ForeignKey(StudentProfile, verbose_name='Ученик', on_delete=models.CASCADE, blank=True,
                                     null=True)
    date = models.DateTimeField('Дата регистрации', auto_now_add=True, blank=True, null=True)
    teacher_name = models.ForeignKey(TeacherProfile, verbose_name='Куратор класса', on_delete=models.CASCADE,
                                     blank=True, null=True)
    lesson = models.ForeignKey(Lesson, verbose_name='Урок', on_delete=models.CASCADE, blank=True, null=True)
    room_num = models.ForeignKey(RoomNum, verbose_name='Кабинет класса', on_delete=models.CASCADE, blank=True,
                                 null=True)
    attendance = models.CharField(verbose_name='Вид', max_length=32, choices=ATTEND_CHOICES)

    def __str__(self):
        return f'{str(self.student_name) | str(self.attendance)}'


class Substitution(models.Model):
    class Meta:
        verbose_name = 'Замена'
        verbose_name_plural = 'Замены'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='substitution',
                                 on_delete=models.CASCADE, blank=True, null=True)

    name_of_lesson = models.CharField(verbose_name='Название Урока', max_length=128, blank=True, null=True)
    teacher_name = models.ForeignKey(TeacherProfile, verbose_name='Куратор класса', on_delete=models.CASCADE,
                                     blank=True, null=True)


class OnlineHomework(models.Model):
    class Meta:
        verbose_name = 'Онлайн ДЗ'
        verbose_name_plural = 'Онлайн ДЗ'

    user_pro = models.ForeignKey(SchoolProfile, verbose_name='Профиль', related_name='onlinehw',
                                 on_delete=models.CASCADE, blank=True, null=True)

    name_of_homework = models.CharField(verbose_name='Тема ДЗ', max_length=128, blank=True, null=True)
    description = models.CharField(verbose_name='Описание', max_length=300, blank=True, null=True)
    link_attach = models.URLField(verbose_name='Сcылка', blank=True, null=True)
    text = models.TextField(verbose_name='Текст Задания', blank=True, null=True)

    def __str__(self):
        return f'{str(self.name_of_homework)}'
