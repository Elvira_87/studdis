from django.apps import AppConfig


class StuddisAppConfig(AppConfig):
    name = 'studdis_app'
