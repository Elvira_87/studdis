
from allauth.account.adapter import get_adapter
from rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from .models import User, SchoolProfile, TeacherProfile, StudentProfile, ClassNum, RoomNum, Lesson


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'username', 'password', 'is_school', 'is_student', 'is_teacher')


class CustomRegisterSerializer(RegisterSerializer):
    is_school = serializers.BooleanField()
    is_student = serializers.BooleanField()
    is_teacher = serializers.BooleanField()

    class Meta:
        model = User
        fields = ('email', 'username', 'password', 'is_school', 'is_student', 'is_teacher')

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'password2': self.validated_data.get('password2', ''),
            'email': self.validated_data.get('email', ''),
            'is_school': self.validated_data.get('is_school', ''),
            'is_student': self.validated_data.get('is_student', ''),
            'is_teacher': self.validated_data.get('is_teacher', '')
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        user.is_school = self.cleaned_data.get('is_school')
        user.is_student = self.cleaned_data.get('is_student')
        user.is_teacher = self.cleaned_data.get('is_teacher')
        user.save()
        adapter.save_user(request, user, self)
        return user


class TokenSerializer(serializers.ModelSerializer):
    user_type = serializers.SerializerMethodField()

    class Meta:
        model = Token
        fields = ('key', 'user', 'user_type')

    def get_user_type(self, obj):
        serializer_data = UserSerializer(
            obj.user
        ).data
        is_school = serializer_data.get('is_school')
        is_student = serializer_data.get('is_student')
        is_teacher = serializer_data.get('is_teacher')
        return {
            'is_school': is_school,
            'is_student': is_student,
            'is_teacher': is_teacher
        }


class SchoolProfileSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = SchoolProfile
        fields = ('user_pro', 'name_of_school', 'address_of_school', 'phone_of_school', 'id')

    def create(self, validated_data):
        user_pro = SchoolProfile.objects.create(**validated_data)
        user_pro.user_pro = self.context['request_user']
        user_pro.save()
        print(self.context['request_user'])
        return user_pro


class TeacherProfileSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = TeacherProfile
        fields = ('user_pro', 'name_of_teacher', 'experience', 'photo', 'lesson', 'id')

    def create(self, validated_data):
        user_pro = TeacherProfile.objects.create(**validated_data)
        user_pro.user_pro = self.context['request_user']
        user_pro.save()
        print(self.context['request_user'])
        return user_pro


class StudentProfileSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = StudentProfile
        fields = ('user_pro', 'photo', 'name_of_student', 'class_num', 'id')

    def create(self, validated_data):
        user_pro = StudentProfile.objects.create(**validated_data)
        user_pro.user_pro = self.context['request_user']
        user_pro.save()
        print(self.context['request_user'])
        return user_pro


class ClassNumSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = ClassNum
        fields = ('user_pro', 'name_of_class', 'class_teacher', 'room', 'id')

    def create(self, validated_data):
        user_pro = ClassNum.objects.create(**validated_data)
        user_pro.user_pro = self.context['request_user']
        user_pro.save()
        print(self.context['request_user'])
        return user_pro


class RoomNumSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = RoomNum
        fields = ('user_pro', 'name_of_room', 'id')

    def create(self, validated_data):
        user_pro = RoomNum.objects.create(**validated_data)
        user_pro.user_pro = self.context['request_user']
        user_pro.save()
        print(self.context['request_user'])
        return user_pro


class LessonSerializer(serializers.ModelSerializer):
    user_pro = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Lesson
        fields = ('user_pro', 'name_of_lesson', 'id')

    def create(self, validated_data):
        user_pro = Lesson.objects.create(**validated_data)
        user_pro.user_pro = self.context['request_user']
        user_pro.save()
        print(self.context['request_user'])
        return user_pro





