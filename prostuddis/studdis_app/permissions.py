from rest_framework.permissions import BasePermission
from .models import SchoolProfile, TeacherProfile, StudentProfile


class IsSchoolProfileOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            profile = SchoolProfile.objects.get(id=view.kwargs['pk'])
            return user == profile.user_pro


class IsTeacherProfileOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            t_prof = TeacherProfile.objects.get(id=view.kwargs['pk'])
            return user == t_prof.profile.user_pro


class IsStudentProfileOwner(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        else:
            user = request.user
            st_prof = StudentProfile.objects.get(id=view.kwargs['pk'])
            return user == st_prof.profile.user_pro
